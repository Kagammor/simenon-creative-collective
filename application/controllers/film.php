<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Film extends CI_Controller {
	public function index()
	{
		$this->breadcrumb->append_crumb('Home', base_url());
		$this->breadcrumb->append_crumb('Film',  base_url('/film'));
	
		$data['content'] = 'film/film'; // View to be requested
		$this->load->view('templates/main', $data);
	}
}

/* END of: ./application/controllers/film.php */
