<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Text extends CI_Controller {
	public function index()
	{
		$this->breadcrumb->append_crumb('Home', base_url());
		$this->breadcrumb->append_crumb('Tekst', base_url('/tekst'));
	
		$data['content'] = 'text/text'; // View to be requested
		$this->load->view('templates/main', $data);
	}
	
	public function literature()
	{
		$this->breadcrumb->append_crumb('Home', base_url());
		$this->breadcrumb->append_crumb('Tekst', base_url('/tekst'));
		$this->breadcrumb->append_crumb('Literatuur', base_url('/tekst/literatuur'));

		$data['content'] = 'text/literature'; // View to be requested
		$this->load->view('templates/main', $data);
	}
}

/* END of: ./application/controllers/literature.php */
