<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webdesign extends CI_Controller {
	public function index()
	{
		$this->breadcrumb->append_crumb('Home', base_url());
		$this->breadcrumb->append_crumb('Webdesign', base_url('/webdesign'));
	
		$data['content'] = 'webdesign/webdesign'; // View to be requested
		$this->load->view('templates/main', $data);
	}
}

/* END of: ./application/controllers/literature.php */
