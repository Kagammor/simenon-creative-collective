<h1>Tekst</h1>

<p>Literatuur is het grondgebied van Harrie Simenon. Hij heeft in de afgelopen jaren gewerkt aan meerdere boeken en korte verhalen,
welke nu eindelijk klaar zijn om gedeeld te worden met de rest van de wereld.</p>

<br>

<ul id="list_sub">
	<li><a href="<?php echo base_url('tekst/literatuur'); ?>">Literatuur</a></li>
</ul>
