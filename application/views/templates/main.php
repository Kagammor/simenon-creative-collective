<!DOCTYPE html>
<html>
	<head>
		<title>Simenon Creative Collective</title>
		
		<link href="<?php echo base_url('assets/css/reset.css'); ?>" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url('assets/css/main.css'); ?>" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url('assets/css/list.css'); ?>" rel="stylesheet" type="text/css">
	</head>
	
	<body>
		<div id="main">
			<img src="<?php echo base_url('assets/img/logo/header.png'); ?>" id="logo" alt="Simenon Creative Collective">
		
			<nav>
				<ul id="navigation">
					<li><a href="<?php echo base_url(); ?>" class="nav_item" id="nav_item_home"></a></li>
					<li><a href="<?php echo base_url('tekst'); ?>" class="nav_item" id="nav_item_literature">Tekst</a></li>
					<li><a href="<?php echo base_url('muziek'); ?>" class="nav_item" id="nav_item_music">Muziek</a></li>
					<li><a href="<?php echo base_url('film'); ?>" class="nav_item" id="nav_item_film">Film</a></li>
					<li><a href="<?php echo base_url('webdesign'); ?>" class="nav_item" id="nav_item_webdesign">Webdesign</a></li>
				</ul>
			</nav>
		
			<div id="breadcrumbs"><?php echo $this->breadcrumb->output(); ?></div>
		
			<div id="content">
				<?php
					// Load content
					$this->load->view($content);
				?>
			</div>
		</div>
	</body>
</html>
