<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		$this->breadcrumb->append_crumb('Home', '/');
	
		$data['content'] = 'home'; // View to be requested
		$this->load->view('templates/main', $data);
	}
}

/* END of: ./application/controllers/home.php */
