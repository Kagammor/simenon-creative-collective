<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Music extends CI_Controller {
	public function index()
	{
		$this->breadcrumb->append_crumb('Home', base_url());
		$this->breadcrumb->append_crumb('Muziek', base_url('/muziek'));
	
		$data['content'] = 'music/music'; // View to be requested
		$this->load->view('templates/main', $data);
	}
}

/* END of: ./application/controllers/music.php */
